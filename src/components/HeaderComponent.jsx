import React from 'react'

export default class HeaderComponent extends React.Component {
    render(){
        return(
            <div><nav className="navbar navbar-expand-lg fixed-top nav-color shadow-sm">
            <div className="container-fluid">
              {/*LOGO*/}
              <a href="index.html" className="navbar-brand"><img class="logoimg" src="images/logo.png" alt="Logo" /></a>
              {/*LOGO ENDS*/}
              {/*Right Widget Part Starts Here*/}
              <div className="float-end pt-2">
                <div className="dropdown mx-2 custom-drop float-start">
                  {/* <a className="user-part ps-3 float-start mt-1 pe-3" href="#"><img src="images/Help.svg" alt="Help-Icon" /></a> */}
                  {/* <p className="p-0 mt-2 float-start text-white User-name">Albert Vincent</p>
                  <a className="user-part ps-3" href="#" id="profilemenu" data-bs-toggle="dropdown" aria-expanded="false"><img src="images/User.svg" alt="User-Icon" /></a> 
                  <ul className="dropdown-menu profilemenu" aria-labelledby="profilemenu">
                    <li><button className="dropdown-item" type="button" data-bs-toggle="modal" data-bs-target="#freetime">Logout</button></li>
                  </ul> */}
                </div>  
              </div>
              {/*Right Widget Part Ends Here*/}
            </div>
          </nav>
          {/*Nav Bar Ends Here*/}</div>
        )
    }
}