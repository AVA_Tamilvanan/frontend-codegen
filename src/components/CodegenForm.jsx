import React from 'react';
import Loader from 'react-loader-spinner';
//PH_FF_01
import validator from 'validator'
import Path from 'path'

import uploadFileToBlob, { isStorageConfigured } from '../util/azure-storage-blob';
import * as FormAction from '../action/CodeGenFormAction';
import FormStore from '../store/Store';
import * as $ from 'jquery';
import queryString from 'query-string';



export default class FormComponent extends React.Component {
  constructor(props) {
    super(props);
    {
      this.state = {
        //PH
        
        //Adding the required state variables as comma seperated values.
        // FileDownload:"D:\\SQLQuery1.sql",
        TeamLeadErrorMsg:'',
        hasError: false,
        ErrorMsg: "",
        isUploading: false,
        ProjectName: "",
        ExcelPath: "",
        fileSelected: "",
        fileSelectedBlob: '',
        FileName: '',
        ModuleName: "",
        TeamLead: '',
        DynTeamLead: [],
        ComponentName: "",
        ComponentType: "-",
        GenerateCodeFor: [],
        DynGenerateCodeFor: [],
        Frontend: 1,
        DynFrontend: [],
        Service: 1,
        DynService: [],
        formData: [],
        Database: 1,
        DynDatabase: [],
        CodeGenID: 0,
        CodeStatus: 0,
        saveClicked: 0,
        GenerateFrontEndhide: 0,
        GenerateServicehide: 0,
        selectedFile: null,
        array:[]



      }
    };
  }

  componentDidMount = () => {
    // this.setState({ isUploading: true })

    //PH_FF_04
    //We call different functions from action for page load and to get data for dynamic fi  elds from the database.
    FormAction.PageLoad();

    FormStore.on('dynamicData', this.loadData);

    const query = this.props.location.search;
    if (query.includes("reqData") || query.includes("colName")) {
      const reqData = new URLSearchParams(query).get('reqData');
      const colName = new URLSearchParams(query).get('colName');
      let ReqData = JSON.stringify( {
        MetaData: reqData,
        ColumnName: colName
      })
      console.log(ReqData)
      this.setState({ CodeGenID: reqData })
      FormAction.getData(ReqData);
      FormStore.on('getDetails', this.loadState);
    }
  }

  //PH_FF_08
  //This is to add a loadState function to dynamically load state variables with values for edit/view.
  loadState = () => {
    var response = FormStore.Response;
    // console.log(response.ResponseData[0]);
    if(FormStore.Response.Success == true)
    {
    this.state.formData.push(response.ResponseData[0][0])
    console.log(this.state.formData);
    // console.log(this.state.DynTeamLead);
    this.state.formData.map((obj, index) => {
      // console.log(obj.FrontendID);
      let samplearray = [];   

      this.setState({
        ProjectName: obj.Project,
        ModuleName: obj.Module,
        TeamLead: obj.ProjectLeadMailID,
        ComponentName: obj.ComponentName,
        ComponentType: obj.ComponentType,
        Frontend: obj.FrontendID[0],
        Service: obj.ServiceID[0],
        Database: obj.DatabaseID[0],
        fileSelectedBlob: "https://avaeuscdegennpstgacc.blob.core.windows.net/ava-cdegen-inp-np-con/" + obj.InputExcelPath,
        FileName: obj.InputExcelPath,
        // DynTeamLead : samplearray

      })
    })
    document.getElementById('Teamlead').value = this.state.TeamLead;

    // document.getElementById('Teamlead').selectedIndex = this.state.TeamLead;
    document.getElementById('Type').selectedIndex = this.state.ComponentType == "Grid" ? 2 : 1;
    if (this.state.Frontend != 1) {
      document.getElementById('frontendCheck1').checked = true;
      this.state.GenerateFrontEndhide = 1;
      this.setState({
        GenerateFrontEndhide: 1
      })
      document.getElementById('Frontend').selectedIndex = this.state.Frontend - 1;
    }
    if (this.state.Service != 1) {
      document.getElementById('serviceCheck2').checked = true;
      this.state.GenerateServicehide = 1;
      this.setState({
        GenerateServicehide: 1
      })

      document.getElementById('Service-type').selectedIndex = this.state.Service - 1;
      document.getElementById('DB-Type').selectedIndex = this.state.Database - 1;
    }
  }
  else {
    this.setState({ isUploading: false })
    this.setState({
      hasError: true,
      ErrorMsg: 'Something went Wrong!!!'//FormStore.Response.ResponseData.ErrorID + "  Something went Wrong!!!"
    })
  }


  }

  //PH_FF_05   
  //This is to add the load and build functions for dynamic data loading.
  loadData = () => {
    debugger;
    var Response = FormStore.Response;
    if(FormStore.Response.Success == true)
    {
      this.setState({
        DynFrontend: Response.ResponseData[0],
        DynService: Response.ResponseData[1],
        DynDatabase: Response.ResponseData[2],


      });
  }
  else {
    this.setState({ isUploading: false })
    this.setState({
      hasError: true,
      ErrorMsg: 'Something went Wrong!!!'//FormStore.Response.ResponseData.ErrorID + "  Something went Wrong!!!"
    })
  }
  }

  // buildTeamLead = () => {
  //   return this.state.DynTeamLead.map((obj, index) => {
  //     return (
  //       <option value={obj.ProjectLeadID}>{obj.ProjectLeadName}</option>
  //     )
  //   })
  // }
  buildGenerateCodeFor = () => {
    return this.state.DynGenerateCodeFor.map((obj, index) => {
      return (
        <div className="optHorizontal">
          <input type="checkbox" className="checkin" name="GenerateCodeFor" id={obj.GenID} onChange={this.handleGenerateCodeFor} />
          <label className="checklabel" htmlFor={obj.GenID}>{obj.GenerateCodeFor}</label>
        </div>
      )
    })
  }
  buildFrontend = () => {
    return this.state.DynFrontend.map((obj, index) => {
      if (index == 1 || index == 3) {
        return (
          <option value={obj.FrontendID}>{obj.FrontendName}</option>
        )
      }
    })
  }
  buildService = () => {
    return this.state.DynService.map((obj, index) => {
      if (index == 2 ) {
        return (
          <option value={obj.ServiceID}>{obj.ServiceName}</option>
        )
      }

    })
  }
  buildDatabase = () => {
    return this.state.DynDatabase.map((obj, index) => {
      if (index == 1 || index == 4) {
        return (
          <option value={obj.DatabaseID}>{obj.DatabaseName}</option>
        )
      }

    })
  }

  handleProjectName = (e) => {
    this.setState({
      ProjectName: e.target.value
    })
  }
  handleModuleName = (e) => {
    this.setState({
      ModuleName: e.target.value
    })
  }
  handleTeamLead = (e) => {
    try {     
        this.setState({
          TeamLead: e.target.value,
          TeamLeadErrorMsg: ""
        
        })     
    }
    catch (error) {
      this.setState({ hasError: true });
      // You can also log the error to an error reporting service
      console.log(error);
    }
  }

onBlurTeamLead = (e) =>{
  try {
    if (validator.isEmail(e.target.value)) {
      this.setState({
        TeamLead: e.target.value,
        TeamLeadErrorMsg: ""
      })
    } else {
      this.setState({
        TeamLeadErrorMsg: "Enter Valid Email"
      })
    }
    
  }
  catch (error) {
    this.setState({ hasError: true });
    // You can also log the error to an error reporting service
    console.log(error);
  }
}

  handleComponentName = (e) => {
    this.setState({
      ComponentName: e.target.value
    })
  }
  handleComponentType = (e) => {
    this.setState({
      ComponentType: e.target.value
    })
  }
  handleGenerateCodeFor = (e) => {

    //this.setState({GenerateCodeFor:[""]})
    console.log(document.getElementsByName("GenerateCodeFor").length)
    for (let i = 0; i < document.getElementsByName("GenerateCodeFor").length; i++) {

      if (document.getElementsByName("GenerateCodeFor")[i].checked == true) {
        if (document.getElementsByName("GenerateCodeFor")[i].id == 'frontendCheck1') {
          this.setState({ GenerateFrontEndhide: 1 })

        }

        if (document.getElementsByName("GenerateCodeFor")[i].id == 'serviceCheck2') {
          this.setState({ GenerateServicehide: 1 })
        }

        // this.state.GenerateCodeFor.push(parseInt(document.getElementsByName("GenerateCodeFor")[i].id));
        // console.log(this.state.GenerateCodeFor);
      }
      else if (document.getElementsByName("GenerateCodeFor")[i].checked == false) {
        if (document.getElementsByName("GenerateCodeFor")[i].id == 'frontendCheck1') {
          this.setState({ GenerateFrontEndhide: 0, Frontend: 1 })
        }

        if (document.getElementsByName("GenerateCodeFor")[i].id == 'serviceCheck2') {
          this.setState({ GenerateServicehide: 0, Service: 1, Database: 1 })
        }


      }
    }
    // console.log(this.state.GenerateCodeFor)
  }

  handleFrontend = (e) => {
    this.setState({
      Frontend: e.target.value
    })
  }
  handleService = (e) => {
    this.setState({
      Service: e.target.value
    })
  }
  handleDatabase = (e) => {
    this.setState({
      Database: e.target.value
    })
  }

  //This is to inlcude required handle functions for input fields

  //PH_FF_06
  //If any of the fields in form are mandatory we need to add validation for those fields.
  validation = () => {
    // console.log(this.state.fileSelected == "" && !this.state.fileSelectedBlob.includes('xlsx'));
    debugger
    // console.log(this.state.GenerateFrontEndhide, this.state.GenerateServicehide);
    if (this.state.ProjectName == "") {
      alert("Enter Project Name");
      } else if (this.state.ModuleName == "") {
        alert("Enter Module Name");
      } else if (this.state.TeamLead == '') {
        alert("Enter Team Lead");
      } else if (this.state.ComponentName == "") {
        alert("Enter Component Name");
      } else if (this.state.ComponentType == "-") {
        alert("Enter Component Type");
      }else if (this.state.GenerateFrontEndhide == 0 && this.state.GenerateServicehide == 0)
      {
        alert('Select any Technology')
      }else if (this.state.Frontend == 1 && !this.state.GenerateFrontEndhide == 0) {
        alert("Enter Frontend ");
      }else if (!this.state.GenerateServicehide == 0 && (this.state.Service == 1 || this.state.Database == 1)) {
       
          alert("Enter Service and Database");
        // }
      }
      else if (this.state.fileSelected == "" && !this.state.fileSelectedBlob.includes('xlsx')) {
        // console.log(this.state.fileSelected);
        alert("Please upload the required excel file");
      }
      else  {
          this.state.saveClicked = 1;
          this.submitClicked();
      // console.log("Asd");
    }
  }

  saveValidation = () => {
    if (this.state.ProjectName == "") {
      alert("Enter Project Name");
    } else if (this.state.ModuleName == "") {
      alert("Enter Module Name");
    } else if (this.state.ComponentName == "") {
      alert("Enter Component Name");
    }
    else {
      this.submitClicked();
    }
  }


  onFileChange = (event) => {
    debugger;
    // capture file into state
    this.setState({ fileSelected: event.target.files[0] });
    if (event.target.files.length == 0) {
      this.setState({ fileSelected: '' });
    }
    this.state.FileName = "";
    // console.log(this.state.fileSelected);
  };

  onFileUpload = async () => {
    debugger;
    // prepare UI
    // this.setState({ uploading: true })
    //setUploading(true);

    // *** UPLOAD TO AZURE STORAGE ***
    const AzureFileName = await uploadFileToBlob(this.state.fileSelected);

    // prepare UI for results
    // this.setSta: blobsInContainer })
    this.state.FileName = AzureFileName;
    //setBlobList(blobsInContainer);
    console.log(this.state.FileName);
    // reset state/form
    this.state.fileSelected = "";//, inputKey: Math.random().toString(36) })

    // this.submitClicked();
    //setFileSelected(null);
    //setUploading(false);
    // setInputKey(Math.random().toString(36));
  }


  submitClicked = async () => {

    //PH_FF_07
    //Every form has a submit button to submit data and if needed, update too.
    //Need to create an object to store the input data and pass it to the database to store in the records.
    // debugger
    this.setState({ isUploading: true })
    // console.log(this.state);
    console.log(this.state.fileSelected, this.state.FileName);
    if (this.state.fileSelected == "" || this.state.FileName != "") {

    } else {

      const AzureFileName = await uploadFileToBlob(this.state.fileSelected);
      // prepare UI for results
      // this.setSta: blobsInContainer })
      this.state.FileName = AzureFileName;
      //setBlobList(blobsInContainer);
      console.log(this.state.FileName);
      // reset state/form
      this.state.fileSelected = "";//, inputKey: Math.random().toString(36) })
    }

    const query = this.props.location.search;
    const reqData = new URLSearchParams(query).get('reqData');

    let postData = JSON.stringify({

      RequiredID: reqData || 0,
      ProjectName: this.state.ProjectName,
      ModuleName: this.state.ModuleName,
      TeamLead: this.state.TeamLead,
      ComponentName: this.state.ComponentName,
      ComponentType: this.state.ComponentType,
      Frontend: this.state.Frontend,
      Service: this.state.Service,
      Database: this.state.Database,
      CodeStatus: this.state.CodeStatus,
      Filename: this.state.FileName

    })
    // console.log(postData);
  
    await FormAction.PostData(postData);
    FormStore.on('postData', () => {
      //console.log(FormStore.Response);

      this.callGoAPI();
    })
  }

  callGoAPI = async () => {
    // this.state.CodeGenID = FormStore.Response.ResponseData[0][0].CodeGenID;
    // console.log(FormStore.Response.ResponseData[0][0].CodeGenID);
    console.log(FormStore.Response);
    if (FormStore.Response.Success == true) {
      this.state.CodeGenID = FormStore.Response.ResponseData[0][0].CodeGenID;
      if (this.state.saveClicked == 1) {
        let GoPostData = JSON.stringify({
          CodeGenID: this.state.CodeGenID,          

          InputPath: `https://avaeuscdegennpstgacc.blob.core.windows.net/ava-cdegen-inp-np-con/${this.state.FileName}`,
          ComponentType: this.state.ComponentType,
          Frontend: this.state.Frontend.toString(),
          Service: this.state.Service.toString(),
          Database: this.state.Database.toString(),
          Filename: this.state.FileName

        })
        // console.log(GoPostData);
        await FormAction.callGoAPI(GoPostData);
        FormStore.on('codegengo', this.codeUpdateNode)
      }
      else {
        this.setState({ isUploading: false })
        window.location.href = "/"

      }

    }
    else {
      
      console.log("Error!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      this.setState({ isUploading: false })
      this.setState({
        hasError: true,
        ErrorMsg: 'Something went Wrong!!!'//FormStore.Response.ResponseData.ErrorID + "  Something went Wrong!!!"
      })
    }

  }

  codeUpdateNode = async () => {
    let resp = JSON.parse(FormStore.Response);
    // console.log(resp);
    // console.log(resp.Success);
    if (resp.Success == true) {
      // window.location.href="/"
      const query = this.props.location.search;
      const reqData = new URLSearchParams(query).get('reqData');
      console.log(FormStore.Response);
      let postData =  JSON.stringify({
        CodeStatus: 1,
        RequiredID: this.state.CodeGenID,
        OutputFilename: resp.ResponseData

      })
      console.log(postData);

      await FormAction.updateFileName(postData);
      FormStore.on('outputfileupdate', () => {
        if (FormStore.Response.Success == true) {
          this.setState({ isUploading: false })
          window.location.href = "/"
        } else if (FormStore.Response.Success == false) {


        }
      })
    }
    else {
      this.setState({ isUploading: false })
      this.setState({
        hasError: true,
        ErrorMsg: resp.ResponseData
      })

    }

  }

  render() {
    //PH_FF_03
    //Render function has all the html code needed and the snippet to enable disable input fields.
    const urldef = queryString.parse(window.location.search)
    console.log((urldef.pMode == "view" || this.state.isUploading));
    if (this.state.isUploading) {
      $("#formId :input").prop("disabled", true);
      $("#saveID").prop("disabled", true);
      $("#GeneratecodeId").prop("disabled", true);


    }
    else {
      $("#formId :input").prop("disabled", false);
      $("#submitbtn").prop("disabled", false);
    }

    return (
      <div>
        {console.log(this.state.ErrorMsg, this.state.hasError)}
        <div hidden={!this.state.hasError} >
          <p style={{
            margin: "15% 100px 0 0",

          }}>
            {this.state.ErrorMsg}
          </p>
        </div>

        <div class={this.state.isUploading ? "blurcontainer" : ""} hidden={this.state.hasError}>
          <link rel="stylesheet" href="css/commontheme.css" />
          <link rel="stylesheet" href="css/stylesheet1.css" />

          {/*Header Starts Here*/}
          <div className="container-fluid CodeGen-Container">
            <form id="formId">
              <div className="row">
                <div className="col-md-12 ps-4">
                  <div className="float-start">
                    <h5 className="header-form float-start pb-2"><a href="/"><img src="Images/Back.svg" alt="back-arrow" className="me-3" title="Back" /></a>Generate New Code</h5>
                  </div>
                </div>
              </div>
              {/*Header Ends Here*/}
              <div className="row justify-content-center">
                <div className="col-md-11 mb-4">
                  <div className="w-100 float-start mb-0">
                    <div className="row">
                      <div className="w-100 float-start p-1 mb-3">
                        <h5 className="Header float-start form-header px-0 mt-4">Project Details</h5>
                      </div>
                    </div>
                  </div>
                  <div className="row w-100">
                    <div className="col-md-4 px-1 mb-4">
                      <label htmlFor="Project-Title" className="Code-label mandatory-label">Project Name</label>
                      <input className="form-control code-input" id="Project-Title" type="text" placeholder="Enter Project Name" aria-label="Disabled input example"
                        value={this.state.ProjectName}
                        onChange={this.handleProjectName} />
                    </div>
                    <div className="col-md-4 px-3 mb-4">
                      <label htmlFor="module" className="Code-label mandatory-label">Module Name</label>
                      <input className="form-control code-input" id="module" type="text" placeholder="Enter Module Name" aria-label="Disabled input example"
                        value={this.state.ModuleName}
                        onChange={this.handleModuleName} />
                    </div>
                    <div className="col-md-4 px-1 mb-4">
                      <label htmlFor="Teamlead" className="Code-label mandatory-label">Team Lead</label>
                      {/* {console.log(this.state.ProjectLeadID, this.state.DynTeamLead)} */}
                      <input id="Teamlead" className="form-control code-input" aria-label="Disabled select example" placeholder="Enter Lead Email" type="email" pattern=".+@avasoft\.com" onChange={this.handleTeamLead} onBlur={this.onBlurTeamLead} />
                      <span style={{
                          fontWeight: 'bold',
                          color: 'red',
                        }}>{this.state.TeamLeadErrorMsg}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row justify-content-center">
                <div className="col-md-11 mb-4">
                  <div className="w-100 float-start mb-0">
                    <div className="row">
                      <div className="w-100 float-start p-1 mb-3">
                        <h5 className="Header float-start form-header px-0 ">Component Details</h5>
                      </div>
                    </div>
                  </div>
                  <div className="row w-100">
                    <div className="col-md-4 px-1 mb-4">
                      <label htmlFor="Component" className="Code-label ">Component Name</label>
                      <input className="form-control code-input" id="Component" type="text" placeholder="Enter Component Name" aria-label="Disabled input example" value={this.state.ComponentName} onChange={this.handleComponentName} />
                    </div>
                    <div className="col-md-4 px-3 mb-4">
                      <label htmlFor="Type" className="Code-label ">Component Type</label>
                      <select id="Type" className="form-select code-input" aria-label="Disabled select example" onChange={this.handleComponentType}>
                        <option selected>Select Component Type</option>
                        <option value="Form">Form</option>
                        <option value="Grid">Grid</option>
                      </select>
                    </div>
                    <div className="col-md-4 px-1 mb-4">
                      <div className="check-content">
                        <label className="Code-label ">Generate Code For</label> 
                      </div>
                      <div className="form-check float-start pt-3">
                        <input className="form-check-input" type="checkbox" name="GenerateCodeFor" defaultValue id="frontendCheck1" onChange={this.handleGenerateCodeFor} />
                        <label className="form-check-label" htmlFor="flexCheckDefault">
                          Frontend</label>
                      </div>
                      <div className="form-check float-start ms-4 pt-3">
                        <input className="form-check-input" type="checkbox" name="GenerateCodeFor" defaultValue id="serviceCheck2" onChange={this.handleGenerateCodeFor} />
                        <label className="form-check-label" htmlFor="Service">
                          Service</label>
                      </div>
                    </div>
                    {this.state.GenerateFrontEndhide == "1" ?
                      <div className="col-md-4 px-1 mb-4">

                        <label htmlFor="Frontend" className="Code-label ">Frontend</label>

                        <select id="Frontend" className="form-select code-input" aria-label="Disabled select example" value={this.state.Frontenddrop} onChange={this.handleFrontend}>
                          <option value="1">Select Frontend</option>
                          {this.buildFrontend()}
                        </select>
                      </div> : null}
                    {this.state.GenerateServicehide == "1" ?
                      // <div className="col-md-4 px-3 mb-4">
                  <div className= {this.state.GenerateFrontEndhide == "1" ? "col-md-4 px-3 mb-4" :  "col-md-4 px-1 mb-4"}>
                        <label htmlFor="Service-type" className="Code-label ">Service</label>
                        <select id="Service-type" className="form-select code-input" aria-label="Disabled select example" onChange={this.handleService}>
                          <option value="1">Select Service</option>
                          {this.buildService()}
                        </select>
                      </div> : null}
                    {this.state.GenerateServicehide == "1" ?
                    <div className= {this.state.GenerateFrontEndhide == "1" ? "col-md-4 px-1 mb-4" :  "col-md-4 px-3 mb-4"}>                                       
                        <label htmlFor="DB-Type" className="Code-label ">Database</label>
                        <select id="DB-Type" className="form-select code-input" aria-label="Disabled select example" onChange={this.handleDatabase}>
                          <option value="1">Select Database</option>
                          {this.buildDatabase()}
                        </select>
                      </div> : null}
                  </div>
                </div>
              </div>
              <div className="row justify-content-center">
                <div className="col-md-11 mb-4">
                  <div className="w-100 float-start mb-0">
                    <div className="row">
                      <div className="w-100 float-start p-1">
                        <h5 className="Header float-start form-header  px-0">Upload File</h5>
                      </div>
                    </div>
                  </div>
                  <div className="row w-100">
                    <div className="col-md-4 px-1 mb-4">
                      <div className="mb-3">
                        <input className="form-control" type="file" id="Upload" accept=".xlsx" onChange={this.onFileChange} />
                        {/* {console.log(this.state.FileName == "" || this.state.CodeGenID==0)}
           {console.log(this.state.FileName ,this.state.CodeGenID)} */}
                        {(this.state.FileName == "" ||this.state.FileName == null|| this.state.CodeGenID == 0) ?
                          this.state.isUploading ? "" :
                            this.state.ComponentType == "Form" ? <a id="downloadID" href={"https://avaeuscdegennpstgacc.blob.core.windows.net/ava-cdegen-tmplt-np-con/template-snippets/Excel Templates/Form Excel.xlsx"}>{"Download Form Template"}</a> : "" ||
                              this.state.ComponentType == "Grid" ? <a id="downloadID" href={"https://avaeuscdegennpstgacc.blob.core.windows.net/ava-cdegen-tmplt-np-con/template-snippets/Excel Templates/Grid Excel.xlsx"}>{"Download Grid Template"}</a> : ""
                          : this.state.isUploading ? "" : <a id="downloadID" href={(this.state.fileSelectedBlob)}>{Path.basename(this.state.fileSelectedBlob)}</a>
                        }{console.log(this.state.ComponentType)}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mb-5">
                <div className="col-md-12">
                  <div className="float-end">
                    <a href="/grid">
                      <button type="button" className="btn btn-light float-start cancel-butt">Cancel</button>
                    </a>

                    {/* <a href="/">  */}
                    <button type="button" className="btn btn-primary save-draft ms-3" id="saveID" onClick={() => this.saveValidation()}>Save as Draft</button>
                    {/* </a> */}

                    {/* <a href="/grid"> */}
                    <button type="button" className="btn btn-primary gen-code ms-3" id="GeneratecodeId" onClick={() => this.validation()}>Generate Code</button>
                    {/* </a > */}

                  </div>
                  {/* <div>
     <Link to={this.state.FileDownload} target="_blank" download>
                                download
                            </Link>
     </div> */}
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="popup"
          style={{
            margin: "15% 0 0 0",
            width: "100%",
            height: "100px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
          hidden={!this.state.isUploading}
        >   
           <Loader type="ThreeDots" color="#023047 " height="150" width="150" />
        </div>


      </div>
    )
  }
}